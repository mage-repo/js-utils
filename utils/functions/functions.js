const {getGrpcOptions} = require('../grpc/server');
const yaml = require('js-yaml');
const fs   = require('fs');
const caller = require('grpc-caller')
const path = require('path')
var protoLoader = require('@grpc/proto-loader');
var grpc = require('@grpc/grpc-js');
const {titleCase} = require('../utils/utils')

function createServiceClients(PROTO_PATH, CONFIG_PATH){

    var service_clients = {}
    const config = yaml.load(fs.readFileSync(CONFIG_PATH, 'utf8'));
    var load = getGrpcOptions(PROTO_PATH)
    config.service.service_connections.map((ele)=>{
        const file = path.join(PROTO_PATH, `/services/${ele.service}_service.proto`)
        const client = caller(`${ele.service}:8080`, {file, load }, `${titleCase(ele.service)}Service`)
        service_clients[ele.service] = client;
    })
    return service_clients
}

function createServer(PROTO_PATH, CONFIG_PATH){

    const config = yaml.load(fs.readFileSync(CONFIG_PATH, 'utf8'));
    var service_name = config.service.name
    var SERVER_PROTO_PATH = `${PROTO_PATH}/services/${service_name}_service.proto`
    var OPTIONS = getGrpcOptions(PROTO_PATH)
    var packageDefinition = protoLoader.loadSync(SERVER_PROTO_PATH, OPTIONS);
    var server_proto = grpc.loadPackageDefinition(packageDefinition).proto_gen.services[`${service_name}service`];
    var server = new grpc.Server();
    return [server, server_proto]
    
}

module.exports = {createServiceClients, createServer}

