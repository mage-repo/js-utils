//let OPTIONS, BASE_PATH, SERVICE_ID, NODE_ID, GEN_PATH, HANDLER_GEN_PATH, MAIN_GEN_PATH

function getGrpcOptions(proto_path){
    return {
        keepCase: true,
        longs: String,
        enums: String,
        defaults: true,
        oneofs: true,
        includeDirs: [ proto_path ]
    }
}

function initGrpcService(configDict, dir){
    var config = {}
    config.GO_PATH = configDict.gopath
    config.BASE_PATH = configDict['base']
    config.SERVICE_ID = configDict['service']
    config.NODE_ID = configDict['node']
    config.CONF_PATH = configDict['config']
    config.GEN_PATH = `${config.GO_PATH}/src/${config.BASE_PATH}/${config.SERVICE_ID}/app/${config.NODE_ID}`
    config.HANDLER_GEN_PATH = `${config.GEN_PATH}/handlers/handlers.js`
    config.TEMPLATE_OBJ = {HANDLER:`${dir}/../templates/handlers/handlers.tjs`, MAIN:`${dir}/../templates/main.tjs`}
    config.MAIN_GEN_PATH = `${config.GEN_PATH}/main.js`
    return config
}

module.exports = {
    getGrpcOptions,
    initGrpcService

}



